FROM node

RUN mkdir /skillbox
WORKDIR /skillbox

COPY package.json /skillbox
COPY yarn.lock /skillbox
RUN yarn install

COPY . /skillbox

RUN yarn build

CMD yarn start
